import { Component, OnInit } from '@angular/core';
import { valuesOfCalculate } from './Models/valuesOfCalculate';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  active = 1;
  title = 'divisionModular';
  arePositive = false;
  inTheRange = false;
  values: valuesOfCalculate ={
    valueA: 0,
    valueB: 0,
    valueN: 0,
  };

  constructor() { }

  ngOnInit(): void { }

  validateValues(values: valuesOfCalculate) {
    this.arePositive = this.validatePositive(values);
    this.inTheRange = this.validateRange(values);

  }

  validatePositive(values: valuesOfCalculate): boolean {
    let validation= true;
     if(values.valueA >= 0 && values.valueB >= 0 && values.valueN >= 0)
     validation=false;
    return validation;
  }

  validateRange(values: valuesOfCalculate): boolean {
    let validation= true;
     if((values.valueA >= 0 && values.valueA < values.valueN) &&
     (values.valueB >= 0 && values.valueB < values.valueN)) {
      validation=false;
     }

    return validation;
  }
}


