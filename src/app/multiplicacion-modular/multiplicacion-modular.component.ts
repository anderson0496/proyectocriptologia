import { Component, Input, OnInit } from '@angular/core';
import { valuesOfCalculate } from '../Models/valuesOfCalculate';

@Component({
  selector: 'app-multiplicacion-modular',
  templateUrl: './multiplicacion-modular.component.html',
  styleUrls: ['./multiplicacion-modular.component.css']
})
export class MultiplicacionModularComponent implements OnInit {


  result: number;
  @Input() values: valuesOfCalculate;
  constructor() { }

  ngOnInit(): void {
  }

  calculate() {
    this.result = (this.values.valueA * this.values.valueB) % this.values.valueN
  }

}
