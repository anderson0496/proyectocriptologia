import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MultiplicacionModularComponent } from './multiplicacion-modular.component';

describe('MultiplicacionModularComponent', () => {
  let component: MultiplicacionModularComponent;
  let fixture: ComponentFixture<MultiplicacionModularComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MultiplicacionModularComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultiplicacionModularComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
