import { Component, Input, OnInit } from '@angular/core';
import { valuesOfCalculate } from '../Models/valuesOfCalculate';

@Component({
  selector: 'app-adicion-modular',
  templateUrl: './adicion-modular.component.html',
  styleUrls: ['./adicion-modular.component.css']
})
export class AdicionModularComponent implements OnInit {

  result: number;
  @Input() values: valuesOfCalculate;
  constructor() { }
  ngOnInit(): void {
  }

  calculate() {
    this.result = (this.values.valueA + this.values.valueB) % this.values.valueN
  }

}
