import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdicionModularComponent } from './adicion-modular.component';

describe('AdicionModularComponent', () => {
  let component: AdicionModularComponent;
  let fixture: ComponentFixture<AdicionModularComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdicionModularComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdicionModularComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
