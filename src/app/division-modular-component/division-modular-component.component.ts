import { Component, Input, OnInit } from '@angular/core';
import { valuesOfCalculate } from '../Models/valuesOfCalculate';

@Component({
  selector: 'app-division-modular-component',
  templateUrl: './division-modular-component.component.html',
  styleUrls: ['./division-modular-component.component.css']
})
export class DivisionModularComponentComponent implements OnInit {

  isReciproco = false
  reciproco: number;
  result: number;
  @Input() values: valuesOfCalculate;
  constructor() { }

  ngOnInit(): void {
  }


  calculateMultiplicacionModular(valueOne, valueTwo): number {
    return (valueOne * valueTwo) % this.values.valueN;
  }

  calculateDivision() {
    this.calculateReciproco();
    let result: number;
    for(let i=0; i<this.values.valueN; i++){
      result = this.calculateMultiplicacionModular(i, this.values.valueB);
      if(this.values.valueA == result) {
        this.result = i;
        break;
      }
    }
  }

  calculateReciproco() {
    let result: number;
    for(let i=0; i<this.values.valueN; i++) {
      result = this.calculateMultiplicacionModular(i, this.values.valueB);
      if(1 == result) {
       this.isReciproco = true
       this.reciproco = i;
        break;
      }
    }
  }


}
