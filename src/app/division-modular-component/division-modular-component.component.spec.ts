import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DivisionModularComponentComponent } from './division-modular-component.component';

describe('DivisionModularComponentComponent', () => {
  let component: DivisionModularComponentComponent;
  let fixture: ComponentFixture<DivisionModularComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DivisionModularComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DivisionModularComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
