import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DivisionModularComponentComponent } from './division-modular-component/division-modular-component.component';

import { FormsModule } from '@angular/forms';
import { AdicionModularComponent } from './adicion-modular/adicion-modular.component';
import { MultiplicacionModularComponent } from './multiplicacion-modular/multiplicacion-modular.component';
import { SustraccionModularComponent } from './sustraccion-modular/sustraccion-modular.component'

@NgModule({
  declarations: [
    AppComponent,
    DivisionModularComponentComponent,
    AdicionModularComponent,
    MultiplicacionModularComponent,
    SustraccionModularComponent
  ],
  imports: [
    BrowserModule,
    NgbModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
