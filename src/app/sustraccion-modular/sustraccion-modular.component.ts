import { Component, Input, OnInit } from '@angular/core';
import { valuesOfCalculate } from '../Models/valuesOfCalculate';

@Component({
  selector: 'app-sustraccion-modular',
  templateUrl: './sustraccion-modular.component.html',
  styleUrls: ['./sustraccion-modular.component.css']
})
export class SustraccionModularComponent implements OnInit {

  result: number;
  @Input() values: valuesOfCalculate;
  constructor() { }

  ngOnInit(): void {
  }

  calculateAdicionModular(valueOne, valueTwo): number {
    return (valueOne + valueTwo) % this.values.valueN
  }

  calculateSustraccion() {
    let result: number;
    for(let i=0; i<this.values.valueN; i++){
      result = this.calculateAdicionModular(i, this.values.valueB);
      if(this.values.valueA == result) {
        this.result = i;
        break;
      }
    }
  }
}
