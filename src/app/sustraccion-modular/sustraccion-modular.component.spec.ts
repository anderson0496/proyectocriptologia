import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SustraccionModularComponent } from './sustraccion-modular.component';

describe('SustraccionModularComponent', () => {
  let component: SustraccionModularComponent;
  let fixture: ComponentFixture<SustraccionModularComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SustraccionModularComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SustraccionModularComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
